﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO.Ports;
using System.Threading;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace Terminal
{
    public partial class Form1 : Form
    {
        SerialPort ComPort = new SerialPort();
        internal delegate void SerialDataReceivedEventHandlerDelegate(object sender, SerialDataReceivedEventArgs e);
        internal delegate void SerialPinChangedEventHandlerDelegate(object sender, SerialPinChangedEventArgs e);
        delegate void SetTextCallback(string text);

        Bitmap bmp;
        Graphics g;

        ThreadStart childref;
        private Thread sendToBufferThread;
        bool flag = true;
        bool startPaint = false;
        int? initX = null;
        int? initY = null;

        public Form1()
        {
            childref = new ThreadStart(SendToBuffer);
            sendToBufferThread = new Thread(childref);

            InitializeComponent();
            g = pnl_Draw.CreateGraphics();
            g.SmoothingMode = SmoothingMode.AntiAlias;
            g.SmoothingMode = SmoothingMode.HighQuality;
            g.SmoothingMode = SmoothingMode.HighSpeed;
        }

        private void ImageToData()
        {
            Rectangle targetBounds = new Rectangle(0, 0, 640, 480);
            bmp = new Bitmap(targetBounds.Width, targetBounds.Height, PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(bmp);
            g.CopyFromScreen(1000, 480, 0, 0, bmp.Size, CopyPixelOperation.SourceCopy);
        }

        public void SendToBuffer()
        {
            try
            {
                int flag = 0;
                if (firstBuffer_radioButton.Checked)
                {
                    flag = 32;
                }
                int color = 0;
                for (int y = 0; y < bmp.Height; y++)
                {
                    for (int x = 0; x < bmp.Width; x++)
                    {
                        Color pixel = bmp.GetPixel(x, y);
                        int red = (pixel.R > 127) ? 1 : 0;
                        int green = (pixel.G > 127) ? 1 : 0;
                        int blue = (pixel.B > 127) ? 1 : 0;
                        color = flag + (red * 4) + (green * 2) + (blue);
                        byte[] b = new byte[1];
                        b[0] = Convert.ToByte(color);
                        ComPort.Write(b, 0, b.Length);
                    }
                }
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread Abort Exception: " + e.Message);
                return;
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine("Thread Abort Exception" + e.Message);
                return;
            }
            finally
            {
                Console.WriteLine("Couldn't catch the Thread Exception");
            }
        }
        
        private void Pnl_Draw_MouseMove(object sender, MouseEventArgs e)
        {
            if(startPaint)
            {
                Pen p = new Pen(btn_PenColor.BackColor,brushSize.Value);
                g.DrawLine(p, new Point(initX ?? e.X, initY ?? e.Y), new Point(e.X, e.Y));
                initX = e.X;
                initY = e.Y;
            }
        }

        private void Pnl_Draw_MouseDown(object sender, MouseEventArgs e)
        {
            startPaint = true;
        }

        private void Pnl_Draw_MouseUp(object sender, MouseEventArgs e)
        {
            startPaint = false;
            initX = null;
            initY = null;
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            ColorDialog c = new ColorDialog();
            if(c.ShowDialog()==DialogResult.OK)
            {
                btn_PenColor.BackColor = c.Color;
            }
        }

        private void NewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            g.Clear(pnl_Draw.BackColor);
            pnl_Draw.BackColor = Color.White;
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(MessageBox.Show("Do you want to Exit?","Exit",MessageBoxButtons.YesNo,MessageBoxIcon.Information)==DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void AboutMiniPaintToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.ShowDialog();
        }

        private void GetPorts_Click(object sender, EventArgs e)
        {
            string[] ArrayComPortsNames = null;
            int index = -1;
            string ComPortName = null;

            ArrayComPortsNames = SerialPort.GetPortNames();
            do
            {
                index += 1;
                cboPorts.Items.Add(ArrayComPortsNames[index]);
            } while (!((ArrayComPortsNames[index] == ComPortName) || (index == ArrayComPortsNames.GetUpperBound(0))));

            Array.Sort(ArrayComPortsNames);

            if (index == ArrayComPortsNames.GetUpperBound(0))
            {
                ComPortName = ArrayComPortsNames[0];
            }
            cboPorts.Text = ArrayComPortsNames[0];
        }

        private void ClosePort_Click(object sender, EventArgs e)
        {
            if (closePort.Text == "Open The Port")
            {
                try
                {
                    ComPort.PortName = Convert.ToString(cboPorts.Text);
                    ComPort.BaudRate = 256000;
                    ComPort.DataBits = 8;
                    ComPort.StopBits = (StopBits)Enum.Parse(typeof(StopBits), "One");
                    ComPort.Handshake = (Handshake)Enum.Parse(typeof(Handshake), "None");
                    ComPort.Parity = (Parity)Enum.Parse(typeof(Parity), "None");
                    ComPort.Open();
                    closePort.Text = "Close The Port";
                }
                catch (UnauthorizedAccessException ue)
                {
                    MessageBox.Show(ue.Message);
                    return;
                }
            }
            else if (closePort.Text == "Close The Port")
            {
                try
                {
                    ComPort.Close();
                    closePort.Text = "Open The Port";

                }
                catch (UnauthorizedAccessException ue)
                {
                    MessageBox.Show(ue.Message);
                    return;
                }
            }
        }

        private void SwitchButton_Click(object sender, EventArgs e)
        {
            if (sendToBufferThread.IsAlive)
            {
                sendToBufferThread.Suspend();
            }

            byte[] b = new byte[1];
            if (firstBuffer_radioButton.Checked)
            {
                b[0] = 192;
            }
            else if (secondBuffer_radioButton.Checked)
            {
                b[0] = 224;
            }

            try
            {
                ComPort.Write(b, 0, b.Length);
            }
            catch (InvalidOperationException er)
            {
                MessageBox.Show(er.Message);
                return;
            }
        }

        private void SendButton_Click(object sender, EventArgs e)
        {
            if (sendToBufferThread.IsAlive)
            {
                sendToBufferThread.Suspend();
            }
            ImageToData();
            byte[] b = new byte[1];
            if (firstBuffer_radioButton.Checked)
            {
                b[0] = 0;
            }
            else if (secondBuffer_radioButton.Checked)
            {
                b[0] = 32;
            }
            try
            {
                ComPort.Write(b, 0, b.Length);
                if (flag)
                {
                    sendToBufferThread.Start();
                    flag = false;
                }
                else
                {
                    sendToBufferThread.Resume();
                }
            }
            catch (InvalidOperationException er)
            {
                MessageBox.Show(er.Message);
                return;
            }
        }

        private void StopButton_Click(object sender, EventArgs e)
        {
            if (sendToBufferThread.IsAlive)
            {
                sendToBufferThread.Suspend();
            }
            byte[] b = new byte[1];
            b[0] = 64;
            try
            {
                ComPort.Write(b, 0, b.Length);
            }
            catch (InvalidOperationException er)
            {
                MessageBox.Show(er.Message);
                return;
            }
        }

        private void ResetButton_Click(object sender, EventArgs e)
        {
            if (sendToBufferThread.IsAlive)
            {
                sendToBufferThread.Suspend();
            }
            ImageToData();
            byte[] b = new byte[1];
            if (firstBuffer_radioButton.Checked)
            {
                b[0] = 128;
            }
            else if (secondBuffer_radioButton.Checked)
            {
                b[0] = 160;
            }
            try
            {
                ComPort.Write(b, 0, b.Length);
            }
            catch (InvalidOperationException er)
            {
                MessageBox.Show(er.Message);
                return;
            }
        }
    }
}
