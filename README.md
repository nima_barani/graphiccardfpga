# Graphics Card implemented on an FPGA

In this project demonstrates how to build a graphics card with an FPGA. This graphics card aims to show images on a screen through a serial bus. In order to transfer data from PC to FPGA, we developed a terminal program in C#. We also added a sub-program to Terminal to draw some paintings and displayed them, immediately, on the display.

In the VHDL part, we designed a state-machine to write the incoming image's data in a Block-RAM and fill buffers which are responsible to display the image on the screen. Also, we implement a protocol to communicate with the Terminal program